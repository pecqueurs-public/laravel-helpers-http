# Laravel Helpers HTTP

## objective

helper to facilitate laravel http client usage from config file.

## Get started

### Install

```bash
composer config repositories.gitlab.com/pecqueurs-public composer https://gitlab.com/api/v4/group/53853709/-/packages/composer/

composer require pecqueurs/laravel-helpers-http
```

### Publish config

One shot :
```bash
php artisan vendor:publish --provider='PecqueurS\LaravelHelpers\Http\Providers\LaravelHelpersHttpProvider' --tag='config' --ansi
```

in composer.json for each update :

```json
...
"scripts": {
    ...
    "post-update-cmd": [
        ...
        "@php artisan vendor:publish --provider='PecqueurS\\LaravelHelpers\\Http\\Providers\\LaravelHelpersHttpProvider' --tag='config' --ansi"
    ],
    ...
},
...
```

### Usage

#### Add config

example config in `./config/http_client.php`: 

```php
'config_name' => [
    'url' => 'xxx', // base url
    'api_key' => 'yyy', // apiKey header
    'bearer' => 'zzz', // authorization bearer token
],
```

#### In code

##### Call

```php
/* Illuminate\Http\Client\Response */
$response = \PecqueurS\LaravelHelpers\Http\Http::from('config_name')->get("/path/to/route");
```

##### Format response

```php
use \PecqueurS\LaravelHelpers\Http\Http;

...

try {
    /* Illuminate\Support\Collection */
    $result = Http::response($response);

    /* array */
    $result = Http::response($response, Http::RESPONSE_TYPE_JSON);

    /* \stdClass */
    $result = Http::response($response, Http::RESPONSE_TYPE_OBJECT);

    /* string */
    $result = Http::response($response, Http::RESPONSE_TYPE_RAW);

} catch (\Throwable $e) {
    // Error on call
}
```

<?php

namespace PecqueurS\LaravelHelpers\Http;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http as HttpFacade;
use Illuminate\Support\Facades\Log;

class Http
{
    const RESPONSE_TYPE_JSON = 'JSON';
    const RESPONSE_TYPE_OBJECT = 'OBJECT';
    const RESPONSE_TYPE_COLLECT = 'COLLECT';
    const RESPONSE_TYPE_RAW = 'RAW';

    static public function from(string $configName): PendingRequest
    {
        $config = config("http_client.$configName");
        if (!$config) {
            throw new \RuntimeException("config for '$configName' is not set");
        }

        $pending = HttpFacade::acceptJson();
        if (isset($config['api_key'])) {
            $pending->withHeaders([
                'apiKey' => $config['api_key'],
            ]);
        }

        if (isset($config['bearer'])) {
            $pending->withToken($config['bearer']);
        }

        if (isset($config['url'])) {
            $pending->baseUrl($config['url']);
        }

        return $pending;
    }

    static public function response(Response $response, string $type = self::RESPONSE_TYPE_COLLECT): array|Collection|\stdClass|string
    {
        if ($response->transferStats) {
            try {
                $url = $response->transferStats->getEffectiveUri()->jsonSerialize();
                $urlCollect = collect(explode('?', $url));
                Log::info('Client time response', [
                    'client_url' => $urlCollect->first(),
                    'client_query' => $urlCollect->count() === 2 ? $urlCollect->last() : null,
                    'status' => $response->status(),
                    'duration' => $response->transferStats->getTransferTime() * 1000
                ]);
            } catch (\Exception $e) {
                Log::error('Unable to log client time response : ' . $e->getMessage());
            }
        }

        $response->throw(function() {
            $reflection = new \ReflectionClass(static::class);
            Log::error('Error on client ' . $reflection->getShortName(), ['http_client_error' => $reflection->getShortName(), 'args' => func_get_args()]);
        });

        if ($type === self::RESPONSE_TYPE_JSON) {
            return $response->json();
        }

        if ($type === self::RESPONSE_TYPE_OBJECT) {
            return $response->object();
        }

        if ($type === self::RESPONSE_TYPE_COLLECT) {
            return $response->collect();
        }

        return $response->body();
    }
}

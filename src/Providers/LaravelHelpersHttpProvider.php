<?php

namespace PecqueurS\LaravelHelpers\Http\Providers;

use Illuminate\Support\ServiceProvider;

class LaravelHelpersHttpProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Config/http_client.php' => config_path('http_client.php'),
        ], 'config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../Config/http_client.php', 'http_client'
        );
    }
}

